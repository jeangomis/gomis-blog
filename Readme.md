1 CREATION DU PROJET
Installation des dépendances de base
Spring web, Mysql Driver, Lombok, Spring Jpa
//
2 STRUCTURE DU PROJET
 Création des packages
    -controller
    -entitity ou model
    -service.impl
    -repository
    -utils
3 CONFIGURATION DE LA BASE DE DONNEES
    -Configuration dans application.properties

spring.datasource.url=jdbc:mysql://localhost:3306/gomis-blog
spring.datasource.username=root
spring.datasource.password=Lys-Kenane2023*
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQLDialect
spring.jpa.hibernate.ddl-auto=update
//
4 CREATION DE L'ENTITY POST
@Entity
// Utilisation de @Data, @AllArgsConstructor, @NoArgsConstructor pour lombok
@Data
@AllArgsConstructor
@NoArgsConstructor


@Entity
@Table(name = "posts", uniqueConstraints = {@UniqueConstraint(columnNames = {"title"})})
public class Post {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
@Column(name ="title", nullable = false)
private String title;
@Column(name ="description", nullable = false)
private String description;
@Column(name ="content", nullable = false)
private String content;
}
//
5 CREATION DU REPOSITORY POUR L'ENTITY POST
public interface PostRepository extends JpaRepository<Post, Long>
//
6 CREATION DE L'EXCEPTION DANS LE PACKAGE EXCEPTION
public class ResourceNotFoundException extends RuntimeException{
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FOUND)
public class ResourceNotFoundException extends  RuntimeException{
@Getter
private String resourceName;
@Getter
private String fieldName;
private String fieldValue;

    public ResourceNotFoundException(String resourceName, String fieldName, String fieldValue) {
        super(String.format("%s not found with %s : '%s'",resourceName,fieldName,fieldValue)); //post not found with is: 1
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
7 CREATION DU PACKAGE PAYLOAD QUI CONTIENT LES DTO

package com.gomis_blog.gomis_blog.payload;

import lombok.Data;

@Data
public class PostDto {
private Long id;
private String title;
private String Description;
private String Content;
}
//CREATION DU POSTSERVICE ET DE SON IMPLEMENTATION 


9 CREATION DU CONTROLLER POST

public class PostController{


}


}



