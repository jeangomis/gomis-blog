package com.gomis_blog.gomis_blog.controller;

import com.gomis_blog.gomis_blog.payload.PostDto;
import com.gomis_blog.gomis_blog.payload.PostResponse;
import com.gomis_blog.gomis_blog.service.PostService;
import com.gomis_blog.gomis_blog.utils.AppConstants;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }
    @PreAuthorize("hasRole('ADMIN')")
    // Create a new post
    @PostMapping
    public ResponseEntity<PostDto> createPost(@Valid @RequestBody PostDto postDto){

       return new ResponseEntity<>( postService.createPost(postDto), HttpStatus.CREATED);
    }
    // Get al post
    @GetMapping
    public PostResponse getAllPosts(
            @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER,required = false) int pageNo,
            @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE,required = false) int pageSize,
            @RequestParam(value = "sortBy",defaultValue = AppConstants.DEFAULT_SORT_BY,required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION,required = false) String sortDir
    ){
        return  postService.getAllPosts(pageNo,pageSize, sortBy, sortDir);
    }
    // Get post by id;
    @GetMapping("/{id}")

    public ResponseEntity<PostDto>getPostById(@PathVariable(value = "id") Long id){

        PostDto postDto = postService.getPostById(id);

        return ResponseEntity.ok(postDto);
    }
    // GET POST BY ID
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public  ResponseEntity<PostDto> updatePost(@Valid @RequestBody PostDto postDto, @PathVariable(value="id")Long id){
        PostDto postResponse = postService.updatePost(postDto, id);

        return new ResponseEntity<>(postResponse,HttpStatus.OK);

    }
    //Delete a post
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String>deletePost(@PathVariable(name = "id")Long id){
        postService.deletePost(id);
        return ResponseEntity.ok("Your post has been deleted");

    }
}
