package com.gomis_blog.gomis_blog.service;

import com.gomis_blog.gomis_blog.payload.LoginDto;
import com.gomis_blog.gomis_blog.payload.RegisterDto;

public interface AuthService {
    String login(LoginDto loginDto);

    String register(RegisterDto registerDto);
}
