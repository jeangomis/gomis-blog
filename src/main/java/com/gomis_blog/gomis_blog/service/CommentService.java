package com.gomis_blog.gomis_blog.service;

import com.gomis_blog.gomis_blog.payload.CommentDto;

import java.util.List;

public interface CommentService {

    CommentDto createComment(Long postId, CommentDto commentDto);
    List<CommentDto> getAllCommentById(long postId);
    CommentDto getCommentById(Long id, Long commentId);
    CommentDto updateComment(Long postId,Long commentId, CommentDto commentRequest);
    void deleteCommentById(Long postId, Long commentId);


}
