package com.gomis_blog.gomis_blog.payload;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class CommentDto {

    private long id;
    // the name should not  be null or  empty
    @NotEmpty(message = "Name should not be empty")
    private String name;
    // the email should not  be null or  empty
    @NotEmpty(message = "Email should not be empty")
    @Email
    private String email;
    // comment body must be minimum 10 characters
    @NotEmpty
    @Size(min = 10, message = "comment body must be minimum 10 characters")
    private String body;

}
