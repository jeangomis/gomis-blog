package com.gomis_blog.gomis_blog.payload;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Set;

@Data
public class PostDto {
    private Long id;
    //title should not be null or empty ;
    //title should have at least 2 characters
    @NotEmpty
    @Size(min = 2, message = "Post title should have at least 2 characters")
    private String title;
    //Post description should not be null or empty ;
    //Post description have at least 10 characters
    @NotEmpty
    @Size(min = 10, message = "Post title should have at least 10 characters")
    private String Description;
    //Post content should not be empty
    @NotEmpty
    private String Content;
    private Set<CommentDto> comments;
}
