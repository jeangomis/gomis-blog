package com.gomis_blog.gomis_blog.repository;

import com.gomis_blog.gomis_blog.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
